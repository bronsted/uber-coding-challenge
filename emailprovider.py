"""
The emailprovider modules provides classes for sending emails reliably through
third party email providers
"""

import abc
import urllib
import urllib2
import base64
import json
import logging


class Message:
    """
    An email message.


    Attributues:
        to: The email address of the receiver of this email - should be a valid 
        email address
        sender: The sender address of the receiver of this email - should be a 
        valid email address
        subject: The subject of the mail
        text: The body of the email

    """

    def __init__(self, to, sender, subject, text):
        self.to = to
        self.sender = sender
        self.subject = subject
        self.text = text

    def is_valid(self):
        """
        Validate this email

        Returns:
            A boolean indicating whether this email is likely to be accepted by 
            third party email service providers as a valid email.
        """
        return \
            self.to is not None and\
            self.sender is not None and\
            self.subject is not None and\
            self.text is not None

    @staticmethod
    def from_dict(dct):
        """
        Message factory method

        Args:
            A string->string dictionary

        Returns:
            Given a dictionary containing the required keys to construct a 
            Message, this method will construct a coresponding Message object 
            and return it. If any keys are missing, None will be returned

        """
        if not ('to' in dct and 'sender' in dct and 'subject' in dct and 'text' in dct):
            return None

        return Message(dct['to'], dct['sender'], dct['subject'], dct['text'])

class EmailProvider:
    """
    A thrird party email service provider

    The EmailProvider is an abstraction over external email service providers. 
    """
    __metaclass__ = abc.ABCMeta


    @abc.abstractmethod
    def send(self, message):
        """
        Sends a message using a concrete email provider

        Args:
            A Message object

        Returns:
            If the message was sent successfully, the method returns a message 
            ID that can be used to track the email - e.g. if the receiver replies
            to the email.

            If, for any reason, the message was not sent, None is returned
        """
        return

class ReliableEmailProvider(EmailProvider):

    """
    An EmailProvider that use a list of EmailProviders to provide a (more) 
    reliable email provider.

    The list of providers defined the order in which the providers are tried

    Attributes:
        providers: the list of EmailProviders
    """

    def __init__(self, providers):
        self.providers = providers

    def send(self, message):
        """
        Send the email using one of the EmailProviders from the providers 
        attribute. The EmailProviders are tried in the order of the providers 
        list until the email is sent or all providers have been tried.

        Returns:
            If an EmailProvider succeds in sending the email, the id received 
            from that provider is returned - otherwise None is returned
        """

        for provider in self.providers:
            res = provider.send(message)
            if res is not None:
                return res

        return None


class MailGunEmailProvider(EmailProvider):
    """
    An EmailProvider using the mailgun service
    """

    urlbase = 'https://api.mailgun.net/v2/'

    def __init__(self, domain, apiKey):
        """
        Constructs a MailGunEmailProvider

        Args:
            domain: the domain to use
            apiKey: The API key to use

        For additional info about the domain and apiKey parameters, see
        http://documentation.mailgun.com/
        """
        username = 'api'
        self._authString = "Basic " +\
            base64.encodestring(
                '%s:%s' % (username, apiKey)).replace('\n', '')
        self.url = self.urlbase + domain + '.mailgun.org/messages'
        logging.info("Created MailGunEmailProvider")

    def send(self, message):
        """
        See super class documentation
        """

        params = urllib.urlencode({
            'from': message.sender,
            'to': message.to,
            'subject': message.subject,
            'text': message.text
        })
        req = urllib2.Request(self.url, params)
        req.add_header("Authorization", self._authString)

        resp = _issue_request(req)
        return None if resp is None else json.load(resp)[u'id'] 

class MandrillEmailProvider(EmailProvider):
    """
    An EmailProvider using the maildrill service

    See https://mandrillapp.com/api/docs/messages.curl.html#method=send for 
    details on the service used
    """

    url = "https://mandrillapp.com/api/1.0/messages/send.json"

    def __init__(self, apiKey):
        """
        Constructs a MandrillEmailProvider

        Args:
            apiKey: the API key for the service
        """

        self.key = apiKey
        logging.info("Created MandrillEmailProvider")

    def send(self, message):
        """
        See super class documentation
        """
        data = json.dumps({
            "key": self.key,
            "message": {
                "from_email": message.sender,
                "text": message.text,
                "to": [
                    {
                        "email": message.to,
                        "type": "to"
                    }
                ],
                "subject": message.subject
            }
        })
        req = urllib2.Request(self.url, data)
        resp = _issue_request(req)
        return None if resp is None else json.load(resp)[0][u'_id'] 

def _issue_request(request):
    """
    Issue request to third party email provider

    This method is used by the concrete email providers to provide consistent
    exception handling.
    """
    try:
        response = urllib2.urlopen(request)
        if (response.getcode() == 200):
            return response

    except urllib2.HTTPError as e:
        logging.warn(
            "The provider failed to servce the request. Got a %s",
            e.code)
        logging.warn("Body:\n%s", e.read())

    except urllib2.URLError as e:
        logging.warn(
            "Failed to connect to %s. Reason: %s",
            request.get_host(), e.reason)

    except Exception as e:
        logging.error(
            "An unexpected exception occured: %s. Reason: %s",
            e, e.reason)

    return None



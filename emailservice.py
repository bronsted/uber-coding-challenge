#!./venv/bin/python
"""
This module implements a reliable email service

The email service aggregates external email service providers by means of a 
ReliableEmailProvider. Per default, the ReliableEmailProvider uses the mandrill 
service and the mailgun service (in that order). To reconfigure the providers 
used (e.g. for testing), change the state of the provider.providers list property 

"""
import flask
from conf import mandrillKey, mailgunDomain, mailgunKey
import emailprovider
from werkzeug.exceptions import BadRequest,UnsupportedMediaType,ServiceUnavailable

from flask import Flask, jsonify, request, abort

app = Flask(__name__)

_sample_message_format = '{"to": "foo@bar.com", "sender":"baz@bar.com", "subject" : "hello", "text": "world"}'

_primary = emailprovider.MandrillEmailProvider(mandrillKey)
_secondary = emailprovider.MailGunEmailProvider(mailgunDomain, mailgunKey)

provider = emailprovider.ReliableEmailProvider([_primary, _secondary])

@app.route('/send', methods = ['POST'])
def send():
    """
    The HTTP entry point for the email service. See README for details
    """
    if not flask.request.json:
        raise UnsupportedMediaType(description="Only application/json supported")

    dct = request.get_json()
    message = emailprovider.Message.from_dict(dct)

    if message is None or not message.is_valid():
        raise BadRequest(description="Provided JSON cannot be deserialized to a valid Message.\nProvided JSON:\n%s\nSample valid data:\n%s" % (jsonify(dct), _sample_message_format))

    res = provider.send(message)
    if res is None:
        raise ServiceUnavailable("No valid provider availeble - try again later")

    return jsonify({"id" : res})

if __name__ == '__main__':
    app.run(debug=True)

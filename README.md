# Uber coding challenge - Email Service

I have implemented a simple email service that aggregates third party email service providers to provide a (more) reliable email service. When a request is received, the aggregated services are requested in turn until sending the email succeeds. In return, the service replies with an email id that can be used to track the email, if the receiver replies to the email. 

The service is implemented in Python using `flask`, and is hosted on Heroku using `gunicorn`. *gevent* is used to allow for parallel processing of requests.

## API and data format:

The service is a single HTTP endpoint `/send` that only accepts POST request's with the content-type `application/json`. Anything else will be rejected with a `400 - BadRequest`. The method is POST because the operation is not idempotent. If the request body contains a JSON dictionary containing the keys `to`, `sender`, `subject`, and `text`, the request is valid and will be served - otherwise a `400 - BadRequest` is returned. Below is listed a valid sample message:

		{
		    "sender": "jeppe@bronsted.net",
		    "to": "jeppe@bronted.net",
		    "subject": "hello",
		    "text": "world"
		}

If the third party email provides succeeds in serving the request, a `200 - OK` with a body containing the message id, will be returned. Below is listed a curl example:

		$ curl -i -H "content-type: application/json" -X POST -d'{"to": "jeppe@bronsted.net", "sender":"jeppe@bronsted.net", "subject" : "hello", "text": "world"}' "http://localhost:8000/send"
		HTTP/1.1 200 OK
		Server: gunicorn/19.0.0
		Date: Thu, 10 Jul 2014 13:07:08 GMT
		Connection: keep-alive
		Content-Type: application/json
		Content-Length: 46

		{
		  "id": "d7fe97e86b6c4d4b871af8ca7eb49394"
		}

If all email providers fail to send the message, a `503 - Service Unavailable` is returned and the client is free to retry the request at a later stage.

Below is listed an overview of the responses to be expected from the service:

Status code  | Description
------------ | -------------
200          | Email was sent - look for message ID in body
415          | Service only supports `application/json`. `Content-Type` header is mandatory
404          | Service is only available at `/send`
405          | Service only supports `POST`
400          | Supplied message cannot be deserialized to a Message. See body for details
503          | Email service not currently available - try again later

## Configuration

The service is configured by updating the `conf.py` file and redeploying the service.

## Design considerations

Below, I have listed some design considerations I have made during the project:

### Idempotence

If the email service fails after sending an email, but before a response is written to the client, the client will have no idea of whether the email was sent successfully or not. The only option would then be to try to resend the email - resulting in the email being sent twice. A way to avoid this scenario, is to make the email service idempotent. This can be done by letting the client generate and include a UUID in the request. When the emailservice has sent the email, it immediately persists the message id and the UUID. If the client then retries the request, the service can return with the id directly (without resending the email). If, however the email service fails before we persist the UUID, we are back to square one. It should be noted that if the third party email services are idempotent (to my knowledge, they are not), we can avoid storing anything and still achieve idempotence.

Depending on what the email service should be used for, I would argue that at-least-once delivery sematics should be sufficient, and that we therefore can leave out support for idempotence.

### Message Queues
Currently, when all email providers are down, the clients are responsible for retrying requests. An alternative could be to include a message queue in the setup. Instead of requesting the service directly, the clients could insert the messages directly into the message queue. Then a daemon could process the messages one by one and send them using the email service and handle the retry strategy. This would enable clients to 'fire and forget'. The message id could be sent back to the clients using the message queue (again, the client would have to include a UUID in the request).

### Alternate failover strategy
Currently, the order in which email providers are tried is static. If provider A is configured to be requested before B, it will always be so (even though it could be that A failed the last 50 requests). A wide range of alternate strategies could be used instead. The providers could, e.g., be given a priority depending on their availability, performance, and cost. In a replicated setup, this priority could be persisted to allow for faster convergence towards an optimal setup.

### Failover of the email service itself
Since the implemented email service is state-less, it can easily be executed concurrently on multiple hosts. This might also improve overall performance.

### Metrics
It would be beneficial to be able to monitor key metrics in the service while it is deployed. This could, e.g., be requests served, time sent serving requests, failed requests, etc. In addition, metrics for each provider could be relevant for monitoring, but also for accounting. Preferably, these metrics should hook into the existing monitoring setup.

### Security
I have considered the email service to be an internal service, to be used by internal agents only. Therefore, there is no security. If the service is to be used from the Internet, security should definitely be added (e.g. using HTTPS)

### Additional Email Providers
To add additional email providers, the email provider module has to be updated. 

### The Message abstraction
The current abstraction of an email Message is currently rather simplistic. It could be expanded with cc and bcc fields as well as support for multiple receivers (in to, cc, and bcc fields). In addition MIME types and attachments could also be supported. 

### Input validation
The input validation performed in the Message class could be expanded to validate provided email addresses by using a regex or a third party lib (e.g. from `django.core.validators`). Currently, there is no email address validation. Therefore, given an invalid email address, the current implementation will try all email providers. This is not optimal. As an alternative, input validation could be delegated to email providers.

### Deployment
It might be a good idea to package the service using setuptools or similar. Since deploying to Heroku is done by pushing code to git, this has not been a consideration.



#!./venv/bin/python
import abc
from time import sleep
import unittest
import emailprovider
import emailservice

import logging
from werkzeug.exceptions import BadRequest,UnsupportedMediaType,InternalServerError
from flask import jsonify

okMessage = emailprovider.Message(
    'jeppe@bronsted.net',
    'jeppe@bronsted.net',
    'testing 123',
    'testing 456')

mandrillKey = "6vu0dOD9xOupJZbTiSzQhQ"
mailgunDomain = 'sandbox222b026d90494c47a76fe29649623a21'
mailgunKey = 'key-5wtjlqxxyj97kq4fnlv0ttlovr1h9j81'

@unittest.skip("skipped to preserve email quota")
class EmailProviderTest:
    """
    Abstract test for EmailProviders

    Concrete implemetations override the create_valid_provider and 
    create_provider_using_wrong_key methods
    """

    __metaclass__ = abc.ABCMeta
    def test_email_ok(self):
        """The emailprovider can send an email"""
        provider = self.create_valid_provider()
        self.assertIsNotNone(provider.send(okMessage))

    def test_email_no_receiver(self):
        """Check that no message is sent if the message has no receiver"""
        message = emailprovider.Message(
            None,
            'jeppe@bronsted.net',
            'testing 123',
            'testing 456')

        provider = self.create_valid_provider()
        self.assertIsNone(provider.send(message))

    def test_email_wrong_key(self):
        """Test that no email is sent if a wrong apiKey (or similar) is 
        provided"""
        mailgun = emailprovider.MailGunEmailProvider(
            mailgunDomain,
            'key-boguskey')

        provider = self.create_provider_using_wrong_key()
        self.assertIsNone(provider.send(okMessage))

    @abc.abstractmethod
    def create_valid_provider(self):
        pass

    def create_provider_using_wrong_key(self):
        pass

class MailGunEmailProviderTest(EmailProviderTest,unittest.TestCase):
    def create_valid_provider(self):
        return emailprovider.MailGunEmailProvider(
            mailgunDomain,
            mailgunKey)

    def create_provider_using_wrong_key(self):
        return emailprovider.MailGunEmailProvider(
            mailgunDomain,
            'key-boguskey')

class MandrillEmailProviderTest(EmailProviderTest,unittest.TestCase):
    def create_valid_provider(self):
        return emailprovider.MandrillEmailProvider(mandrillKey)

    def create_provider_using_wrong_key(self):
        return emailprovider.MandrillEmailProvider("boguskey")

class MockEmailProvider(emailprovider.EmailProvider):
    """
    A mock email provider used for testing

    The send message will return the argument given in the constructor - unless
    the constructor arguement is an exception - in that case, it will throw an
    exception.
    """

    def __init__(self, result):
        self.result = result
        self.was_called = False

    def send(self, message):
        self.was_called = True
        if isinstance(self.result, Exception):
            raise self.result
        return self.result

class DelayingEmailProvider(MockEmailProvider):
    """
    A MockEmailProvider with delay

    This provider will wait for delay seconds before sending an email.
    delay is specified in the constructor
    """
    def __init__(self, result, delay):
        super(DelayingEmailProvider, self).__init__(result)
        self.delay = delay

    def send(self, message):
        sleep(self.delay)
        return super(DelayingEmailProvider, self).send(message)

class ReliableEmailProviderTest(unittest.TestCase):

    def test_primary_is_ok(self):
        """
        Check that an email is sent if the primary provider is ok
        """
        primaryProvider = MockEmailProvider(u'42')
        secondaryProvider = MockEmailProvider(None)
        providers = [primaryProvider, secondaryProvider]
        reliableProvider = emailprovider.ReliableEmailProvider(providers)

        res = reliableProvider.send(okMessage)
        self.assertTrue(primaryProvider.was_called)
        self.assertFalse(secondaryProvider.was_called)
    
    def test_primary_returns_none(self):
        """
        Check that the secondary provider is tried if the primary is not 
        available
        """
        primaryProvider = MockEmailProvider(None)
        secondaryProvider = MockEmailProvider(u'42')
        providers = [primaryProvider, secondaryProvider]
        reliableProvider = emailprovider.ReliableEmailProvider(providers)

        res = reliableProvider.send(okMessage)
        self.assertEquals(res, u'42')
        self.assertTrue(primaryProvider.was_called)
        self.assertTrue(secondaryProvider.was_called)

    def test_fail_over_only_used_on_failure(self):
        """
        Check that the secondary provider is not tried if the primary is
        available
        """
        primaryProvider = MockEmailProvider(u'42')
        secondaryProvider = MockEmailProvider(u'foo')
        providers = [primaryProvider, secondaryProvider]
        reliableProvider = emailprovider.ReliableEmailProvider(providers)

        res = reliableProvider.send(okMessage)
        self.assertEquals(res, u'42')
        self.assertTrue(primaryProvider.was_called)
        self.assertFalse(secondaryProvider.was_called)

class EmailServiceTest(unittest.TestCase):
    """
    Test the EmailService
    """

    def setUp(self):
        emailservice.app.config['TESTING'] = True
        self.app = emailservice.app.test_client()
        emailservice.provider.providers = []
            
    def test_wrong_content_type(self):
        """Wrong content type results in 415"""
        res = self.app.post('/send', data = "fd")
        self.assertEquals(415, res.status_code)

    def test_wrong_method(self):
        """GET results in 405"""
        res = self.app.get('/send', data = "fd")
        self.assertEquals(405, res.status_code)

    def test_wrong_path(self):
        """Wrong path results in 404"""
        res = self.app.get('/sendsd', data = "fd")
        self.assertEquals(404, res.status_code)

    def test_unparsable_json(self):
        """Unparsable json results in 400"""
        res = self.app.post('/send', headers = [('Content-Type', 'application/json')], data = "fd = }")
        self.assertEquals(400, res.status_code)

    def test_missing_message_property(self):
        """Missing keys results in 400"""
        data = '{"to" : "jeppe@bronsted.net"}'
        res = self.app.post('/send', headers = [('Content-Type', 'application/json')], data = data)
        self.assertEquals(400, res.status_code)
        pass

    def test_no_providers(self):
        """No providers results in 503"""
        data = '{"to" : "jeppe@bronsted.net", "sender" : "jeppe@bronsted.net", "subject" : "foo", "text" : "bar"}'
        res = self.app.post('/send', headers = [('Content-Type', 'application/json')], data = data)
        self.assertEquals(503, res.status_code)

    def test_no_ok_providers(self):
        """No ok providers results in 503"""
        emailservice.provider.providers = [MockEmailProvider(None)]
        data = '{"to" : "jeppe@bronsted.net", "sender" : "jeppe@bronsted.net", "subject" : "foo", "text" : "bar"}'
        res = self.app.post('/send', headers = [('Content-Type', 'application/json')], data = data)
        self.assertEquals(503, res.status_code)

    def test_ok_provider(self):
        """If there is on ok provider, 200 is returned"""
        emailservice.provider.providers = [MockEmailProvider(u'42')]
        data = '{"to" : "jeppe@bronsted.net", "sender" : "jeppe@bronsted.net", "subject" : "foo", "text" : "bar"}'
        res = self.app.post('/send', headers = [('Content-Type', 'application/json')], data = data)
        self.assertEquals(200, res.status_code)
        self.assertTrue('42' in res.data)

if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s %(levelname)-6s %(message)s',
        level=logging.INFO,
        filename="test.log")
    unittest.main()

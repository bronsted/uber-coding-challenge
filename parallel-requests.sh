#!/bin/bash

if [ $# -eq 0 ] ; then
	echo "Usage: $(basename $0) <parallel requests> <host>"
	exit 0 
fi

P="$1"
HOST="$2"

echo "$(date) Start"
START=$(date +%s)
for i in $(seq 1 $P) ; do
	(curl -s -H "content-type: application/json" -X POST -d'{"to": "jeppe@bronsted.net", "sender":"jeppe@bronsted.net", "subject" : "hell", "text": "world"}' "http://$HOST/send" > /dev/null 2>&1 ; printf "$(date) Process %2s done\n" $i ) &
	sleep 0.01
done

wait
END=$(date +%s)
echo "$(date) Done. Elapsed $((END - START)) seconds"
